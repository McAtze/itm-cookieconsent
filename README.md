# -ITM- Cookie Consent
[![StyleCI](https://styleci.io/repos/81040271/shield?branch=master)](https://styleci.io/repos/81040271)
[![Average time to resolve an issue](http://isitmaintained.com/badge/resolution/McAtze/-ITM-CookieConsent.svg)](http://isitmaintained.com/project/McAtze/-ITM-CookieConsent "Average time to resolve an issue")
[![Percentage of issues still open](http://isitmaintained.com/badge/open/McAtze/-ITM-CookieConsent.svg)](http://isitmaintained.com/project/McAtze/-ITM-CookieConsent "Percentage of issues still open")

### An Xenforo add-on for the Cookie Consent by Insites

With this little add-on you be able to insert and adjust the [Cookie Consent](https://github.com/insites/cookieconsent/) by Insites into your Xenforo Forum. No more template edits or deletions after upgrade processes.

If you wanna thank me for this add-on just [PayPal.Me](https://www.paypal.me/itmaku) ..

### Installation

There is nothing to upload. Just install the addon.xml through the acp.

#### Upgrade 1.0.1 => 1.0.2

First uninstall the old version and then install the new one.

### Screenshots

![Options](https://maxcdn.it-maku.com/git/cc/options.png)
![Style Properties](https://maxcdn.it-maku.com/git/cc/style_properties.png)
![Top Classic](https://maxcdn.it-maku.com/git/cc/top_classic.png)
![Top (Pushdown) Classic](https://maxcdn.it-maku.com/git/cc/top_pushdown_classic.png)
![Bottom Classic](https://maxcdn.it-maku.com/git/cc/bottom_classic.png)
![Bottom Edgeless](https://maxcdn.it-maku.com/git/cc/bottom_edgeless.png)
![Bottom Wire](https://maxcdn.it-maku.com/git/cc/bottom_wire.png)
![Floating Left Classic](https://maxcdn.it-maku.com/git/cc/floating_left_classic.png)
![Floating Right Classic](https://maxcdn.it-maku.com/git/cc/floating_right_classic.png)
![Floating Right Block](https://maxcdn.it-maku.com/git/cc/floating_right_block.png)
![Floating Right Edgeless](https://maxcdn.it-maku.com/git/cc/floating_right_edgeless.png)
![Floating Right Wire](https://maxcdn.it-maku.com/git/cc/floating_right_wire.png)

### Documentation 

See the [full documentation](https://cookieconsent.insites.com/documentation/).

### License

Code released under the [MIT licence](http://opensource.org/licenses/MIT).
